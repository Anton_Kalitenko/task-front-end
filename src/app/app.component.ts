import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {invalid} from "@angular/compiler/src/render3/view/util";

@Component({
  selector: 'form-component',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{
  title = 'Angular';
  checkoutForm = this.formBuilder.group({
    firstName: ['', [Validators.required, Validators.minLength(3)]],
    lastName: ['', [Validators.required]],
    address: '',
    address2: '',
    city: '',
    stateProvince: '',
    postalCode: '',
    phoneNumber: '',
    email: ['', [Validators.email]]
  });
  constructor(
    private formBuilder: FormBuilder,
  ) {}
  invalid: boolean=false
  onSubmit(): void {
    if(this.checkoutForm.valid){
      console.log(this.checkoutForm.value)
      this.invalid = false
    }else{
      this.invalid = true
    }
  }
}
